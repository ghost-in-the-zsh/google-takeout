
from google_takeout import google_takeout as takeout
from google_takeout import google_takeout_hangouts as hangouts

def main_takeout():
    takeout.main()

def main_hangouts():
    hangouts.main()
