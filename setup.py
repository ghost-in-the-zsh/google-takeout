#!/usr/bin/env python3

from os.path import join, dirname

from setuptools import setup, find_packages

from google_takeout.settings import VERSION


REQUIRED_PYTHON = (3, 6)


def readme():
    with open(join(dirname(__file__), 'README.md')) as f:
        return f.read()


setup(
    name='google-takeout',
    version=VERSION,
    python_requires='>={}.{}'.format(*REQUIRED_PYTHON),
    description='Google Takeout data export reader',
    long_description=readme(),
    long_description_content_type='text/markdown',
    author='Raymond L. Rivera',
    author_email='ray.l.rivera@gmail.com',
    maintainer='Raymond L. Rivera',
    maintainer_email='ray.l.rivera@gmail.com',
    url='https://gitlab.com/ghost-in-the-zsh/google-takeout',
    packages=find_packages(exclude=['google_takeout.tests.*']),
    entry_points={
        'console_scripts': [
            'google-takeout=google_takeout.entrypoints:main_takeout',
            'google-takeout-hangouts=google_takeout.entrypoints:main_hangouts'
        ],
    },
    keywords='google takeout reader hangouts',
    classifiers=(
        # https://pypi.org/classifiers/
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Other Audience',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3 Only',
        'Topic :: Text Processing :: General',
        'Topic :: Utilities'
    ),
)
