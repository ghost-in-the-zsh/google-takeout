# How to Contribute

Stick to the following guidelines and you should be fine:

1. Follow [PEP 8](https://www.python.org/dev/peps/pep-0008/) guidelines.
2. Sign your work.


## Sign Your Work - The Developer's Certificate of Origin

To track authorship of the work, you _must_ include a "sign-off" line in your commit messages.

The sign-off is a simple line at the end of the explanation for the patch, which certifies that you wrote it or otherwise have the right to pass it on as a free/libre software patch. The rules are pretty simple: if you can certify the following[^2]:

> #### Developer's Certificate of Origin 1.1
>
> By making a contribution to this project, I certify that:
>
> (a) The contribution was created in whole or in part by me and I
>     have the right to submit it under the open source license
>     indicated in the file; or
>
> (b) The contribution is based upon previous work that, to the best
>     of my knowledge, is covered under an appropriate open source
>     license and I have the right under that license to submit that
>     work with modifications, whether created in whole or in part
>     by me, under the same open source license (unless I am
>     permitted to submit under a different license), as indicated
>     in the file; or
>
> (c) The contribution was provided directly to me by some other
>     person who certified (a), (b) or (c) and I have not modified
>     it.
>
> (d) I understand and agree that this project and the contribution
>     are public and that a record of the contribution (including all
>     personal information I submit with it, including my sign-off) is
>     maintained indefinitely and may be redistributed consistent with
>     this project or the open source license(s) involved.

then you just add a line saying:

    Signed-off-by: Random J Developer <random.developer@example.org>

using your real name. (No pseudonyms or anonymous contributions will be accepted.) Adding the `-s` option to `git commit` will include the sign-off line automatically.

[^2]: https://developercertificate.org/
